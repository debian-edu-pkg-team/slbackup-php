{*
    slbackup-php, an administration tool for slbackup
    Copyright (C) 2007  Finn-Arne Johansen <faj@bzz.no>, Bzzware AS, Norway

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*}
<html>
    <head>
<link rel="stylesheet" href="sl-style.css" type="text/css">

	<title>{t}SLBackup Configuration{/t}</title>
    </head>
    <body>
<div class="menu">
  <h2 class="menu">Menu</h2>

  <form name=menu method=post>
    <ul>
        <li><A HREF=index.php?status=submit>{t}Status{/t}</A></li>
        <li><A HREF=index.php?config=submit>{t}Config{/t}</A></li>
        <li><A HREF=index.php?restore=submit>{t}Restore{/t}</A></li>
        <li><A HREF=index.php?maint=submit>{t}Maintenance{/t}</A></li>
        <li><A HREF=index.php?logout=submit>{t}Logout{/t}</A></li>
   </ul>

  </form>
</div>
